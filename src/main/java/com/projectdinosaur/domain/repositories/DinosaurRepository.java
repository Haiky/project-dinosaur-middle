package com.projectdinosaur.domain.repositories;

import com.projectdinosaur.domain.model.Dinosaur;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "dinosaurs", path = "dinosaurs")
public interface DinosaurRepository extends Neo4jRepository<Dinosaur, Long> {
}

