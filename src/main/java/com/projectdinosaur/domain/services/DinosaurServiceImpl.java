package com.projectdinosaur.domain.services;

import com.projectdinosaur.domain.model.Dinosaur;
import com.projectdinosaur.domain.repositories.DinosaurRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DinosaurServiceImpl implements DinosaurService {

    private final DinosaurRepository dinosaurRepository;

    public DinosaurServiceImpl(DinosaurRepository dinosaurRepository) {
        this.dinosaurRepository = dinosaurRepository;
    }

    @Override
    public List<Dinosaur> getDinosaurs() {
        List<Dinosaur> dinosaurs = new ArrayList<>();
        for (Dinosaur d : this.dinosaurRepository.findAll()) {
            dinosaurs.add(d);
        }
        return dinosaurs;
    }
}