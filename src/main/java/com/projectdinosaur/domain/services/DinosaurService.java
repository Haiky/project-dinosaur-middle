package com.projectdinosaur.domain.services;

import com.projectdinosaur.domain.model.Dinosaur;

import java.util.List;

public interface DinosaurService {
    List<Dinosaur> getDinosaurs();
}
