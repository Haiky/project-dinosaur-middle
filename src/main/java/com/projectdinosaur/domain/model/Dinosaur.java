package com.projectdinosaur.domain.model;

import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity
public class Dinosaur {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String description;
    private String image;

    public Dinosaur() {
    }

    public Dinosaur(String name, String description, String image) {
        this.name = name;
        this.description = description;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }
}
