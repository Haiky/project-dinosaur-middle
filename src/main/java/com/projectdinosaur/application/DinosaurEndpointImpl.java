package com.projectdinosaur.application;

import com.projectdinosaur.domain.model.Dinosaur;
import com.projectdinosaur.domain.services.DinosaurService;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DinosaurEndpointImpl implements DinosaurEndpoint {

    private final DinosaurService dinosaurService;

    public DinosaurEndpointImpl(DinosaurService dinosaurService) {
        this.dinosaurService = dinosaurService;
    }


    @Override
    public List<Dinosaur> getDinosaurs() {
        return this.dinosaurService.getDinosaurs();
    }

    @Override
    public Dinosaur getDinosaurByName(String name) {
        return null;
    }
}
