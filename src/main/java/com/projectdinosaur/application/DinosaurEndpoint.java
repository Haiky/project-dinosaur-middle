package com.projectdinosaur.application;

import com.projectdinosaur.domain.model.Dinosaur;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/dinosaur")
public interface DinosaurEndpoint {

    @GetMapping("/dinosaurs")
    List<Dinosaur> getDinosaurs();

    @GetMapping("/{name}")
    Dinosaur getDinosaurByName(String name);
}
